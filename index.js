
const amqp = require('amqplib/callback_api');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

MongoClient.connect('mongodb://mongo:27017/myproject', function(err, db) {
  assert.equal(null, err);
  console.log("Connected successfully to MongoDB");

  db.close();
});

amqp.connect('amqp://rabbitmq', function(err, conn) {
  assert.equal(null, err);
  console.log("Connected successfully to RabbitMQ");

  conn.close();
});

setTimeout(function() { process.exit(0) }, 10000);
